// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "rcss3d_controller/rcss3d_controller.hpp"

#include <netinet/tcp.h>
#include <cmath>
#include <string>
#include <utility>
#include <algorithm>

using namespace std::chrono_literals;

namespace rcss3d_controller
{

Rcss3DController::Rcss3DController()
: rclcpp::Node{"rcss3d_controller"}
{
  // Read parameters
  auto host = std::string{};
  auto port = int32_t{};

  get_parameter_or("rcss3d/host", host, std::string{"127.0.0.1"});
  get_parameter_or("rcss3d/port", port, 3100);
  get_parameter_or("team", team_, std::string{"Anonymous"});
  get_parameter_or("unum", unum_, 0u);

  get_parameter_or("imu_frame", imu_frame_, std::string{"base_link"});

  // Set up connection
  initSocket(host, port);
  connect();

  // Create the robot
  create();
  state_ = State::CREATING;

  // Publishers
  clock_pub_ = create_publisher<Clock>("/clock", 10);
  imu_pub_ = create_publisher<Imu>("/imu/data_raw", 10);
  joint_state_pub_ = create_publisher<JointState>("/joint_states", 10);

  // Subscriptions
  joint_command_sub_ =
    create_subscription<JointCommand>(
    "/joint_commands",
    10,
    [this](JointCommand::SharedPtr cmd) {
      RCLCPP_DEBUG(get_logger(), "Got joint commands, for effectors:");
      for (auto const & n : cmd->name) {
        RCLCPP_DEBUG(get_logger(), n.c_str());
      }

      auto sexp = sexpresso::Sexp{};
      for (auto i = 0u; i < cmd->name.size(); ++i) {
        auto jointSexp = sexpresso::Sexp{cmd->name[i]};
        jointSexp.addChild(std::to_string(cmd->speed[i]));
        sexp.addChild(std::move(jointSexp));
      }
      send(sexp, false);
    });

  // Main loop
  receive_thread_ = std::thread(
    [this]() {
      while (rclcpp::ok() && !canceled_.load()) {
        // Read new message
        RCLCPP_DEBUG(get_logger(), "Starting receive");
        auto len = receive();
        RCLCPP_DEBUG(get_logger(), "Received: %u", len);
        if (len == 0) {
          return;
        }

        switch (state_) {
          case State::CREATING:
            // Robot is created, initialize it
            RCLCPP_INFO(get_logger(), "Created ==> Initializing");
            init();
            state_ = State::INITIALIZING;
            break;

          case State::INITIALIZING:
            // Robot initialized, ready to run
            RCLCPP_INFO(get_logger(), "Initialized ==> Running");
            state_ = State::RUNNING;
            // Parse and handle incoming message
            handle();
            break;

          case State::RUNNING:
            // Parse and handle incoming message
            handle();
            break;
        }
      }
    });
}

Rcss3DController::~Rcss3DController()
{
  canceled_.store(true);
  if (receive_thread_.joinable()) {
    receive_thread_.join();
  }
}

void Rcss3DController::initSocket(std::string const & host, int port)
{
  socket_.reset(new Socket(PF_INET, SOCK_STREAM, 0));
  socket_address_.reset(new SocketAddress(PF_INET, port, host));
}

void Rcss3DController::connect()
{
  socket_->connect(*socket_address_);
  if (*socket_) {
    RCLCPP_INFO(get_logger(), "Connected to server");
    initConnection();
  } else {
    RCLCPP_ERROR(get_logger(), "Failed connecting to server");
  }
}

void Rcss3DController::initConnection()
{
  socket_->setBlocking(true);
  socket_->setsockopt(IPPROTO_TCP, TCP_NODELAY, true);
}

uint32_t Rcss3DController::receive()
{
  buffer_.reserve(4);
  auto len = socket_->readExactly(buffer_.data(), 4);
  if (len != 4) {
    RCLCPP_ERROR(get_logger(), "Failed reading prefix");
    return 0;
  }

  auto prefix = int32_t{};
  std::copy(buffer_.begin(), std::next(buffer_.begin(), 4), reinterpret_cast<char *>(&prefix));
  prefix = ntohl(prefix);

  buffer_.reserve(prefix + 1);
  len = socket_->readExactly(buffer_.data(), prefix);
  if (len != uint64_t(prefix)) {
    RCLCPP_ERROR(get_logger(), "Failed reading prefix");
    return 0;
  }
  // Ensure string is 0-terminated
  buffer_[prefix] = 0;
  return prefix;
}

void Rcss3DController::send(sexpresso::Sexp sexp, bool wrap)
{
  auto root = sexpresso::Sexp{};
  if (wrap) {
    root.addChild(std::move(sexp));
  } else {
    root = std::move(sexp);
  }

  auto msg = root.toString();
  RCLCPP_DEBUG(get_logger(), "Sending: %s", msg.c_str());

  auto len = htonl(msg.length());

  auto prefix = std::string{reinterpret_cast<const char *>(&len), sizeof(unsigned int)};
  auto data = prefix + msg;

  socket_->writeExactly(data.c_str(), data.length());
}

void Rcss3DController::create()
{
  auto sceneSexp = sexpresso::Sexp{"scene"};
  sceneSexp.addChild("rsg/agent/nao/nao.rsg");
  send(sceneSexp);
}

void Rcss3DController::init()
{
  auto initSexp = sexpresso::Sexp{"init"};

  auto unumSexp = sexpresso::Sexp{"unum"};
  unumSexp.addChild(std::to_string(unum_));
  initSexp.addChild(std::move(unumSexp));

  auto teamSexp = sexpresso::Sexp{"team"};
  teamSexp.addChild(team_);
  initSexp.addChild(std::move(teamSexp));

  send(initSexp);
}

void Rcss3DController::handle()
{
  auto msg = std::string{buffer_.data()};
  RCLCPP_DEBUG(get_logger(), "Received: %s", msg.c_str());
  auto sexp = sexpresso::parse(msg);

  // Clock
  auto rcss3d_now = std::stod(sexp.getChildByPath("time/now")->value.sexp[1].value.str);
  auto clock = rosgraph_msgs::msg::Clock{};
  clock.clock.sec = std::floor(rcss3d_now);
  clock.clock.nanosec = (rcss3d_now - clock.clock.sec) * 1e9;
  clock_pub_->publish(clock);

  // IMU
  auto const * gyrSexp = sexp.getChildByPath("GYR/rt");
  auto const * accSexp = sexp.getChildByPath("ACC/a");
  if (gyrSexp != nullptr && accSexp != nullptr) {
    auto imuMsg = sensor_msgs::msg::Imu{};
    imuMsg.header.stamp = clock.clock;
    imuMsg.header.frame_id = imu_frame_;

    auto const & rateSexp = gyrSexp->value.sexp;
    imuMsg.angular_velocity.x = deg2rad(std::stod(rateSexp[2].value.str));
    imuMsg.angular_velocity.y = -deg2rad(std::stod(rateSexp[1].value.str));
    imuMsg.angular_velocity.z = deg2rad(std::stod(rateSexp[3].value.str));

    auto const & aSexp = accSexp->value.sexp;
    imuMsg.linear_acceleration.x = std::stod(aSexp[2].value.str);
    imuMsg.linear_acceleration.y = -std::stod(aSexp[1].value.str);
    imuMsg.linear_acceleration.z = std::stod(aSexp[3].value.str);

    RCLCPP_DEBUG(get_logger(), "Publishing IMU message");
    imu_pub_->publish(imuMsg);
  }

  // Joints
  auto joint_states = sensor_msgs::msg::JointState{};
  joint_states.header.stamp = clock.clock;
  for (auto const & arg : sexp.arguments()) {
    // Joint expressions have form: (HJ (n llj2) (ax -0.00))
    auto const & s = arg.value.sexp;
    if (s[0].value.str == "HJ") {
      joint_states.name.push_back(s[1].value.sexp[1].value.str);
      joint_states.position.push_back(deg2rad(std::stod(s[2].value.sexp[1].value.str)));
    }
  }
  joint_state_pub_->publish(joint_states);
}

}  // namespace rcss3d_controller
